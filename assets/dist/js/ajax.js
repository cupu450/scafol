$(document).ready(function(){

  $('#make_request').on('submit', function(event){
    event.preventDefault();
    $.ajax({
       url: BaseUrl+'user/makeareq',
       method:"POST",
       data:new FormData(this),
       contentType:false,
       cache:false,
       processData:false,
       beforeSend:function(){
          $('#myloading').fadeIn();
        },
       success:function(result){
         $('#myloading').fadeOut();
         Swal.fire({
           icon: 'success',
           title: 'Your work has been saved',
           showConfirmButton: true,
           allowOutsideClick: false
         }).then((result) => {
           if (result.value) {
             location.reload();
           }
         })
       }
    })
  });

  $('#formCheckedLaporan').on('submit', function(event){
    event.preventDefault();
    $.ajax({
       url: BaseUrl+'user/checkedReport',
       method:"POST",
       data:new FormData(this),
       contentType:false,
       cache:false,
       processData:false,
       beforeSend:function(){
          $('#myloading').fadeIn();
        },
       success:function(){
         $('#myloading').fadeOut();
         Swal.fire({
           icon: 'success',
           title: 'Your work has been saved',
           showConfirmButton: true,
           allowOutsideClick: false
         }).then((result) => {
           if (result.value) {
             location.reload();
           }
         })
       }
    })
  });

  $('#changePass').on('submit', function(event){
    event.preventDefault();
    $.ajax({
       url: BaseUrl+'user/change_pass',
       method:"POST",
       data:new FormData(this),
       contentType:false,
       cache:false,
       processData:false,
       beforeSend:function(){
          $('#myloading').fadeIn();
        },
       success:function(result){
         var data = JSON.parse(result);

         $('#myloading').fadeOut();
         if (data.hasil == 'fail') {
           Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: data.error.not_found
            });
         } else {
           Swal.fire({
             icon: 'success',
             title: 'Password berhasil diubah',
             showConfirmButton: true,
             allowOutsideClick: false
           }).then((result) => {
             if (result.value) {
               location.reload();
             }
           })
         }
       }
    })
  });

})
