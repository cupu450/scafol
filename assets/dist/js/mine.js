$(document).ready(function(){
  $('#tgl_awal').daterangepicker({
    singleDatePicker: true,
    locale: {
      format: 'YYYY-MM-DD'
    }
  });

   $('#tgl_akhir').daterangepicker({
      singleDatePicker: true,
      locale: {
        format: 'YYYY-MM-DD'
      }
    });

  $('#kategoriBrg').change(function() {
      $('#nmBrg').removeClass('hide');
  });

  $('.btn-track').click(function() {
    var tracking = $(this).attr('data-track').split("");
    var tanggal = $(this).attr('data-tgl');
    var html = '';


    html += '<div class="time-label">'+
              '<span class="bg-green">'+tanggal+'</span>'+
            '</div>';

    for(let i = 0; i < tracking.length; i++){
      if(tracking[i] == 0){var txt = 'Permintaan telah dikirim ke kepala sub bagian umum';}
      if(tracking[i] == 1){var txt = 'Permintaan Telah diterima oleh kepala sub bagian umum';}
      if(tracking[i] == 2){var txt = 'Permintaan Telah diserahkan kepada Pak Priyono';}
      if(tracking[i] == 3){var txt = 'Permintaan Telah diterima oleh Pak Priyono';}
      if(tracking[i] == 4){var txt = 'Kerusakan Telah dicek oleh Pak Priyono';}
      if(tracking[i] == 5){var txt = 'Kerusakan Telah diperbaiki';}
      if(tracking[i] == 6){var txt = 'Permintaan Perbaikan telah diselesaikan dan sudah bisa dicetak oleh kepala sub bagian umum';}

      html += '<div>'+
                '<i class="fas fa-clock bg-blue"></i>'+
                '<div class="timeline-item">'+
                  '<h4 class="timeline-header no-border">'+txt+'</h4>'+
                '</div>'+
              '</div>';
    }
    $('#show_track').html(html);
    $('#modal-track').modal('show');
  });

  $("#sifatPerbaikan").change(function(){
    if ($("#sifatPerbaikan option:selected").val() == "Pihak 3") {
      $("#NmPihak3").removeClass("hide");
      document.getElementById('NmPihak3').value = null;
    } else {
      $("#NmPihak3").addClass("hide");
      $("#NmPihak3").val("NULL");
    }
  });

  $('#btnCekKerusakan').click(function() {
    if($('#descPerbaikan').val() == ''){
      toastr.error('form deskripsi perbaikan yang diperlukan tidak boleh kosong');
    }else if ($("#sifatPerbaikan option:selected").val() == '') {
      toastr.error('form sifat perbaikan tidak boleh kosong');
    } else if($('#NmPihak3').val() == ''){
      toastr.error('form Nama Pihak Ketiga tidak boleh kosong');
    } else {
      $.ajax({
         url: BaseUrl+'user/upd_cek_kerusakan',
         method:"POST",
         data:{'id_permintaan':$('#idPerbaikan').val(), 'id_teknisi':$('#idTeknisi').val(), 'instruksi':$('#instruksi_diterima').val(), 'desc_perbaikan':$('#descPerbaikan').val(), 'sifat_perbaikan':$("#sifatPerbaikan option:selected").val(), 'nama_pihak_ketiga':$('#NmPihak3').val()},
         success:function(result){
           Swal.fire({
             icon: 'success',
             title: 'Kerusakan Telah diperbaiki silahkan mengisi data yang kosong untuk mengisi catatan hasil perbaikan',
             showConfirmButton: true,
             allowOutsideClick: false
           }).then((result) => {
             if (result.value) {
               location.reload();
             }
           })
         }
      })
    }
  });

  $('#btnHasilPerbaikan').click(function() {
    if($('#descHasilPerbaikan').val() == ''){
      toastr.error('form catatan hasil perbaikan tidak boleh kosong');
    } else{
      $.ajax({
         url: BaseUrl+'user/upd_catatan_perbaikan',
         method:"POST",
         data:{'id_permintaan':$('#idPerbaikan').val(), 'id_teknisi':$('#idTeknisi').val(), 'catatan':$("#descHasilPerbaikan").val()},
         success:function(result){
           Swal.fire({
             icon: 'success',
             title: 'catatan hasil perbaikan telah berhasil dikirim',
             showConfirmButton: true,
             allowOutsideClick: false
           }).then((result) => {
             if (result.value) {
               location.reload();
             }
           })
         }
      })
    }
  });

  // $('.swalDefaultSuccess').click(function() {
  //     Swal.fire({
  //       icon: 'success',
  //       title: 'Your work has been saved',
  //       showConfirmButton: true,
  //       allowOutsideClick: false
  //     }).then((result) => {
  //       if (result.value) {
  //         location.reload();
  //       }
  //     })
  //   });
});

function CekPerbaikan(id,bidang,ruangan,kategori,nama_brg,kerusakan,nama_kabid,tanggal,upd_tu){
  if (upd_tu == 0) {
    $.ajax({
       url: BaseUrl+'user/upd_tgl_tu',
       method:"POST",
       data:{'id':id},
       success:function(result){
         Swal.fire({
           icon: 'success',
           title: 'Data Permintaan Perbaikan berhasil di terima, silahkan klik tombol ok kemudian klik tombol cek data lagi untuk mengisi data yang kosong',
           showConfirmButton: true,
           allowOutsideClick: false
         }).then((result) => {
           if (result.value) {
             location.reload();
           }
         })
       }
    })
  } else {
    $('#txtBidang').html(bidang);
    $('#txtRuangan').html(ruangan);
    $('#txtKategori').html(kategori);
    $('#txtBarang').html(nama_brg);
    $('#txtketKerusakan').html(kerusakan);
    $('#txtKabid').html(nama_kabid);
    $('#txtTgl').html(tanggal);
    $('#val_perbaikan').val(id);
    $('#val_tu').val(upd_tu);

    $('#modal-cek-tu').modal('show');
  }

}

function CekPerbaikanTeknisi(id,bidang,ruangan,kategori,nama_brg,kerusakan,nama_kabid,tanggal,status,upd_teknisi,tgl_laporan,diserahkan_kepada,instruksi){
  if (upd_teknisi == 0) {
    $.ajax({
       url: BaseUrl+'user/upd_tgl_teknisi',
       method:"POST",
       data:{'id':id},
       success:function(result){
         Swal.fire({
           icon: 'success',
           title: 'Data Permintaan Perbaikan berhasil di terima, silahkan klik tombol ok kemudian klik tombol Cek Kerusakan untuk mengisi data yang kosong',
           showConfirmButton: true,
           allowOutsideClick: false
         }).then((result) => {
           if (result.value) {
             location.reload();
           }
         })
       }
    })
  } else {
    $('#txtBidang').html(bidang);
    $('#txtRuangan').html(ruangan);
    $('#txtKategori').html(kategori);
    $('#txtBarang').html(nama_brg);
    $('#txtketKerusakan').html(kerusakan);
    $('#txtKabid').html(nama_kabid);
    $('#txtTgl').html(tanggal);
    $('#tglCek').html(tgl_laporan);
    $('#txtDiserahkan').html(diserahkan_kepada);
    $('#txtInstruksi').html(instruksi);
    $('#idPerbaikan').val(id);
    $('#idTeknisi').val(upd_teknisi);

    if (status == 3) {
      $('#descHasilPerbaikan').attr('disabled',true);
      $('#btnHasilPerbaikan').attr('disabled',true);
    } else if (status == 4) {
      $('#btnCekKerusakan').attr('disabled',true);
      $('#descPerbaikan').attr('disabled',true);
      $('#sifatPerbaikan').attr('disabled',true);
    }

    $('#modal-cek-teknisi').modal('show');
  }

}

function btnFinishing(id)
{
  $.ajax({
     url: BaseUrl+'user/upd_finishing',
     method:"POST",
     data:{'id':id},
     success:function(result){
       Swal.fire({
         icon: 'success',
         title: 'Permintaan perbaikan telah diselesaikan silahkan print data setelah klik ok',
         showConfirmButton: true,
         allowOutsideClick: false
       }).then((result) => {
         if (result.value) {
           location.reload();
         }
       })
     }
  })
}
