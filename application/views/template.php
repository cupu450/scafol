<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Scafol Indonesia | Internship Program</title>
	<link rel="icon" href="<?= base_url().'assets/dist/img/iconscafol.png' ?>">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
	<link rel="stylesheet" href="<?= base_url().'assets/plugins/fontawesome-free/css/all.min.css' ?>">
	<link rel="stylesheet" href="<?= base_url().'assets/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css' ?>">
	<link rel="stylesheet" href="<?= base_url().'assets/dist/css/adminlte.min.css' ?>">
	<link rel="stylesheet" href="<?= base_url().'assets/plugins/summernote/summernote-bs4.min.css' ?>">
	<link rel="stylesheet" href="<?= base_url().'assets/dist/css/style.css' ?>">
</head>

<body class="hold-transition layout-top-nav">
	<div id="myloading" style="display:none"></div>

	<div class="wrapper">

		<?= $_navbar ?>

		<div class="content-wrapper">
      		<?= $_content ?>
		</div>

		<?= $_footer ?>

	</div>


	<script src="<?= base_url().'assets/plugins/jquery/jquery.min.js' ?>"></script>
	<script src="<?= base_url().'assets/plugins/bootstrap/js/bootstrap.bundle.min.js' ?>"></script>
	<script src="<?= base_url().'assets/plugins/sweetalert2/sweetalert2.min.js' ?>"></script>
	<script src="<?= base_url().'assets/dist/js/adminlte.min.js' ?>"></script>
	<script src="<?= base_url().'assets/plugins/summernote/summernote-bs4.min.js' ?>"></script>
	<script src="<?= base_url().'assets/plugins/filterizr/jquery.filterizr.min.js' ?>"></script>
	<script>
		var BaseUrl = '<?= base_url() ?>';
		$('.deskripsi').summernote();

		$(document).ready(function(){
			load_data();

			$('#filter_kategori').on('change', function(){
				$.ajax({
					url: BaseUrl+'get_data',
					method:"POST",
					data:{'kategori' : $(this).val()},
					beforeSend:function(){
						$('#myloading').fadeIn();
					},
					success:function(response){
						var data = JSON.parse(response);
						var html = '';
						var i;
						$('#myloading').fadeOut();
						for (i = 0; i < data.data.length; i++) {
							html += '<div class="col-md-12 mb-4">'+
										'<div class="card discuss d-flex h-100">'+
											'<div class="card-body">'+
												'<div class="row">'+
													'<div class="col-md-1 justify-content-center align-self-center">'+
														'<img src="'+BaseUrl+'/assets/dist/img/'+data.data[i].foto+'" class="img-circle img-fluid">'+
													'</div>'+
													'<div class="col-md-9 justify-content-center align-self-center">'+
														'<h6>Pak Budi <label class="text-scafol"> - ('+data.data[i].tanggal+')</label></h6>'+
														'<p>'+data.data[i].judul+'</p>'+
													'</div>'+
													'<div class="col-md-2 text-center justify-content-center align-self-center">'+
														'<div class="row">'+
															'<div class="col-md-6 text-right">'+
																'<a href="#" class="komen_act" onclick="CommentAct('+data.data[i].Id_diskusi+');"><i class="fas fa-comment-dots text-scafol"></i></a>'+
															'</div>'+
														'</div>'+
													'</div>'+
												'</div>'+
											'</div>'+
										'</div>'+
									'</div>';
						}
						
						$('#load_discuss').html(html);
						$('#load_discuss').removeClass('hide');
						$('#load_comment').addClass('hide');
						
					}
				});
			});

			$('#filter_title').on('change', function(){
				$.ajax({
					url: BaseUrl+'get_data',
					method:"POST",
					data:{'judul' : $(this).val()},
					beforeSend:function(){
						$('#myloading').fadeIn();
					},
					success:function(response){
						var data = JSON.parse(response);
						var html = '';
						var i;
						$('#myloading').fadeOut();
						for (i = 0; i < data.data.length; i++) {
							html += '<div class="col-md-12 mb-4">'+
										'<div class="card discuss d-flex h-100">'+
											'<div class="card-body">'+
												'<div class="row">'+
													'<div class="col-md-1 justify-content-center align-self-center">'+
														'<img src="'+BaseUrl+'/assets/dist/img/'+data.data[i].foto+'" class="img-circle img-fluid">'+
													'</div>'+
													'<div class="col-md-9 justify-content-center align-self-center">'+
														'<h6>Pak Budi <label class="text-scafol">- ('+data.data[i].tanggal+')</label></h6>'+
														'<p>'+data.data[i].judul+'</p>'+
													'</div>'+
													'<div class="col-md-2 text-center justify-content-center align-self-center">'+
														'<div class="row">'+
															'<div class="col-md-6 text-right">'+
																'<a href="#" class="komen_act" onclick="CommentAct('+data.data[i].Id_diskusi+');"><i class="fas fa-comment-dots text-scafol"></i></a>'+
															'</div>'+
														'</div>'+
													'</div>'+
												'</div>'+
											'</div>'+
										'</div>'+
									'</div>';
						}

						$('#load_discuss').html(html);
						$('#load_discuss').removeClass('hide');
						$('#load_comment').addClass('hide');
						
					}
				});
			});

			$('#add-disscussion').on('submit', function(event){
				event.preventDefault();
				$.ajax({
					url: BaseUrl+'add_disscussion',
					method:"POST",
					data:new FormData(this),
					contentType:false,
					cache:false,
					processData:false,
					success:function(response){
						var data = JSON.parse(response);

						if (data.success) {
							Swal.fire({
								icon: 'success',
								title: 'Congratulations!',
								text: data.message,
								showConfirmButton: true,
								allowOutsideClick: false
							}).then((result) => {
								if (result.value) {
									location.reload();
								}
							})
						} else {
							Swal.fire({
								icon: 'error',
								title: 'Oops...',
								text: data.message
							});
						}
					}
				})
			});

			$('#add-comment').on('submit', function(event){
				event.preventDefault();
				$.ajax({
					url: BaseUrl+'add_comment',
					method:"POST",
					data:new FormData(this),
					contentType:false,
					cache:false,
					processData:false,
					success:function(response){
						var data = JSON.parse(response);

						if (data.success) {
							Swal.fire({
								icon: 'success',
								title: 'Congratulations!',
								text: data.message,
								showConfirmButton: true,
								allowOutsideClick: false
							}).then((result) => {
								if (result.value) {
									$('#modal-add-comment').modal('hide');
									CommentAct(data.id_diskusi);
								}
							})
						} else {
							Swal.fire({
								icon: 'error',
								title: 'Oops...',
								text: data.message
							});
						}
					}
				})
			});

			$('#reply-comment').on('submit', function(event){
				event.preventDefault();
				$.ajax({
					url: BaseUrl+'reply_comment',
					method:"POST",
					data:new FormData(this),
					contentType:false,
					cache:false,
					processData:false,
					success:function(response){
						var data = JSON.parse(response);
						if (data.success) {
							Swal.fire({
								icon: 'success',
								title: 'Congratulations!',
								text: data.message,
								showConfirmButton: true,
								allowOutsideClick: false
							}).then((result) => {
								if (result.value) {
									$('#modal-reply-comment').modal('hide');
									CommentAct(data.id_diskusi);
								}
							})
						} else {
							Swal.fire({
								icon: 'error',
								title: 'Oops...',
								text: data.message
							});
						}
					}
				})
			});

			$('.filter-container').filterizr({
                gutterPixels: 3
            });
            $('.btn[data-filter]').on('click', function() {
                $('.btn[data-filter]').removeClass('active');
                $(this).addClass('active');
            });

			$('.btn-photo').on('click', function(){
				$('#txt_desc').html($(this).attr('data-desc'));
				$('#txt_user').html($(this).attr('data-user'));
				$('#txt_tag').html($(this).attr('data-category'));
				$('#modal-photo .foto_gallery').attr("src", BaseUrl+"assets/dist/img/"+$(this).attr('data-img'));
				
				$('#modal-photo').modal('show');
				$('#just_view').fadeIn();
				$('#form_view').fadeOut();
			});

		});

		function load_data() {
			$.ajax({
				url: BaseUrl+'get_data',
				type: 'POST',
				data:{'judul' : '', 'kategori' : ''},
				success: function(response) {
					var data = JSON.parse(response);
					var html = '';
					var i;
					// console.log(data);
					for (i = 0; i < data.data.length; i++) {
						html += '<div class="col-md-12 mb-4">'+
									'<div class="card discuss d-flex h-100">'+
										'<div class="card-body">'+
											'<div class="row">'+
												'<div class="col-md-1 justify-content-center align-self-center">'+
													'<img src="'+BaseUrl+'/assets/dist/img/'+data.data[i].foto+'" class="img-circle img-fluid">'+
												'</div>'+
												'<div class="col-md-9 justify-content-center align-self-center">'+
													'<h6>Pak Budi <label class="text-scafol">- ('+data.data[i].tanggal+')</label></h6>'+
													'<p>'+data.data[i].judul+'</p>'+
												'</div>'+
												'<div class="col-md-2 text-center justify-content-center align-self-center">'+
													'<div class="row">'+
														'<div class="col-md-6 text-right">'+
															'<a href="#" class="komen_act" onclick="CommentAct('+data.data[i].Id_diskusi+');"><i class="fas fa-comment-dots text-scafol"></i></a>'+
														'</div>'+
													'</div>'+
												'</div>'+
											'</div>'+
										'</div>'+
									'</div>'+
								'</div>';
					}

					$('#load_discuss').html(html);
				}
			});
		}

		function CommentAct(id)
		{
			$.ajax({
				url: BaseUrl+'get_comment',
				method:"POST",
				data:{'id' : id},
				beforeSend:function(){
					$('#myloading').fadeIn();
				},
				success:function(response){
					$('#myloading').fadeOut();
					$('#load_discuss').addClass('hide');
					$('#load_comment').removeClass('hide');
					$('#load_comment').html(JSON.parse(response));
				}
			});
		}

		function CallCommentModal(id)
		{
			$('#id_diskusi').val(id);	
			$('#modal-add-comment').modal('show');
		}

		function CallReplyComment(id_komentar, id_diskusi)
		{
			$('#id_komentar').val(id_komentar);
			$('#id_diskusi_rep').val(id_diskusi);	
			$('#modal-reply-comment').modal('show');
		}

		function GoBack()
		{
			location.reload();
		}
		
		function editView()
		{
			$('#form_view').fadeIn();
			$('#just_view').fadeOut();
		}
	</script>
  
</body>
</html>
