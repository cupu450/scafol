<div class="content-header">
	<div class="container">
		<div class="row mb-2">
			<div class="col-sm-12">
				<h1 class="m-0 text-dark"> Gallery <small>Page</small></h1>
			</div>
		</div>
	</div>
</div>

<div class="content">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="card card-primary">
					<div class="card-header">
						<h4 class="card-title">FilterizR Gallery with Ekko Lightbox</h4>
					</div>
					<div class="card-body">
						<div>
							<div class="btn-group w-100 mb-2">
								<a class="btn btn-info active" href="javascript:void(0)" data-filter="all"> All items </a>
								<a class="btn btn-info" href="javascript:void(0)" data-filter="Jalan Tol"> Jalan Tol </a>
								<a class="btn btn-info" href="javascript:void(0)" data-filter="Jembatan"> Jembatan </a>
								<a class="btn btn-info" href="javascript:void(0)" data-filter="Underpass"> Underpass </a>
							</div>
						</div>
						<div>
							<div class="filter-container p-0 row">
								<?php
									foreach($gallery as $row):
										echo'
											<div class="filtr-item col-sm-2" data-category="'.$row['tag'].'" data-sort="'.$row['tag'].'">
												<a href="#" class="btn-photo" data-toggle="lightbox" data-category="'.$row['tag'].'" data-img="'.$row['image'].'" data-desc="'.$row['desc'].'"  data-user="'.$row['user'].'">
													<img src="'.base_url().'assets/dist/img/'.$row['image'].'" class="img-fluid mb-2" alt="white sample" />
												</a>
											</div>
										';
									endforeach;
								?>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-photo">
	<div class="modal-dialog modal-xl">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Extra Large Modal</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
        <div id="form_view" style="display:none;">
          <label>ini form edit</label>
        </div>
        
        <div id="just_view">
          <div class="row">
            <div class="col-md-6" style="padding-right: 25px">
              <img class="foto_gallery" width="100%">
            </div>
            <div class="col-md-6" style="padding-left: 25px; border-left: 2px solid #000">
              <div class="row">
                <div class="col-md-6">
                  <h5 id="txt_user"></h5>
                  <h6 id="txt_tag" style="font-size: 14px; margin-bottom: 20px"></h6>
                </div>
                <div class="col-md-6">
                  <button class="btn btn-lg btn-default float-right" data-toggle="dropdown" aria-expanded="false" style="font-weight:900;background:transparent; border: transparent">...</button>
                  <div class="dropdown-menu" role="menu" style="">
                    <a class="dropdown-item" href="#" onclick="editView();" ><span class="fa fa-edit"></span> Edit</a>
                    <a class="dropdown-item" href="#"><span class="fa fa-trash"></span> Hapus</a>
                  </div>
                </div>
                <div class="col-md-12">
                  <p id="txt_desc" class="text-justify"></p>
                </div>
              </div>
            </div>
          </div>
        </div>
			</div>
			<div class="modal-footer justify-content-between">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Save changes</button>
			</div>
		</div>
	</div>
</div>