<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>

	<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body {
		margin: 0 15px 0 15px;
	}

	p.footer {
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	#container {
		margin: 10px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
	}

	#myloading {
	position: fixed;
	top: 0;
	left: 0;
	right: 0;
	bottom: 0;
	z-index: 9999;
	overflow: hidden;
	background: #fff;
	}

	#myloading:before {
	content: "";
	position: fixed;
	top: calc(50% - 30px);
	left: calc(50% - 30px);
	border: 6px solid #f2f2f2;
	border-top: 6px solid #18d26e;
	border-radius: 50%;
	width: 60px;
	height: 60px;
	-webkit-animation: animate-preloader 1s linear infinite;
	animation: animate-preloader 1s linear infinite;
	}

	#myloading h3{
	top: calc(60% - 30px);
	left: calc(48% - 31px);
	position: fixed;
	font-size: 18px;
	font-weight: 400;
	}

	@-webkit-keyframes animate-preloader {
	0% {
		-webkit-transform: rotate(0deg);
		transform: rotate(0deg);
	}

	100% {
		-webkit-transform: rotate(360deg);
		transform: rotate(360deg);
	}
	}

	@keyframes animate-preloader {
	0% {
		-webkit-transform: rotate(0deg);
		transform: rotate(0deg);
	}

	100% {
		-webkit-transform: rotate(360deg);
		transform: rotate(360deg);
	}
	}

	#filter_kategori
	{
		height: 30px;
    	width: 100%;
    	border-color: turquoise;
		margin-bottom: 15px;
	}

	#filter_title{
		height: 25px;
		width: 99%;
		margin-bottom: 15px;
	}
	</style>
</head>
<body>
<div id="myloading" style="display:none"></div>
<div id="container">
	<h1>Welcome to CodeIgniter!</h1>

	<div id="body">
		<select id="filter_kategori">
			<option selected disabled>Filter By Kategori</option>
			<option>Teknologi</option>
			<option>Industri</option>
			<option>Politik</option>
		</select>

		<input type="text" id="filter_title" placeholder="Masukan judul">

		<div id="load_discuss"></div>
	</div>

	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
</div>

<script src="<?= base_url().'assets/plugins/jquery-3.5.1/jquery.min.js' ?>"></script>
<script>
	var BaseUrl = '<?= base_url() ?>';

	$(document).ready(function(){
		load_data();
		$('#filter_kategori').on('change', function(){
			$.ajax({
				url: BaseUrl+'get_data',
				method:"POST",
				data:{'kategori' : $(this).val()},
				beforeSend:function(){
					$('#myloading').fadeIn();
				},
				success:function(response){
					var data = JSON.parse(response);
					var html = '';
					var i;
					$('#myloading').fadeOut();
					for (i = 0; i < data.data.length; i++) {
						html += '<code>'+
								'<h3>'+data.data[i].judul+' <small>kategori <strong>'+data.data[i].kategori+'</strong></small></h3>'+
								'<p>'+data.data[i].deskripsi+'</p>'+
								'<a href="'+BaseUrl+'komentar/'+data.data[i].Id_diskusi+'">Komentar</a>'+
								'</code>';
					}

					$('#load_discuss').html(html);
					
				}
			});
		});

		$('#filter_title').on('change', function(){
			// console.log($(this).val());
			$.ajax({
				url: BaseUrl+'get_data',
				method:"POST",
				data:{'judul' : $(this).val()},
				beforeSend:function(){
					$('#myloading').fadeIn();
				},
				success:function(response){
					var data = JSON.parse(response);
					var html = '';
					var i;
					$('#myloading').fadeOut();
					for (i = 0; i < data.data.length; i++) {
						html += '<code>'+
								'<h3>'+data.data[i].judul+' <small>kategori <strong>'+data.data[i].kategori+'</strong></small></h3>'+
								'<p>'+data.data[i].deskripsi+'</p>'+
								'<a href="'+BaseUrl+'komentar/'+data.data[i].Id_diskusi+'">Komentar</a>'+
								'</code>';
					}

					$('#load_discuss').html(html);
					
				}
			});
		});

	});

	function load_data() {
		$.ajax({
			url: BaseUrl+'get_data',
			type: 'POST',
			data:{'judul' : '', 'kategori' : ''},
			success: function(response) {
				var data = JSON.parse(response);
				var html = '';
				var i;
				// console.log(data);
				for (i = 0; i < data.data.length; i++) {
					html += '<code>'+
							'<h3>'+data.data[i].judul+' <small>kategori <strong>'+data.data[i].kategori+'</strong></small></h3>'+
							'<p>'+data.data[i].deskripsi+'</p>'+
							'<a href="'+BaseUrl+'komentar/'+data.data[i].Id_diskusi+'">Komentar</a>'+
							'</code>';
				}

				$('#load_discuss').html(html);
			}
		});
	}
</script>

</body>
</html>