<div class="content-header">
	<div class="container">
		<div class="row mb-2">
			<div class="col-sm-12">
				<h1 class="m-0 text-dark"> Discuss <small>Page</small></h1>
			</div>
		</div>
	</div>
</div>

<div class="content">
	<div class="container">
		<div class="row">
            <div class="col-md-12 mb-3">
                <div class="row">
                    <div class="col-md-2">
                        <button class="btn btn-flat btn-sm btn-scafol" data-toggle="modal" data-target="#modal-add-disscussion">Tambah Diskusi</button>
                    </div>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="filter_title" placeholder="Cari ...">
                    </div>
                    <div class="col-md-2">
                        <select class="form-control" id="filter_kategori">
                            <option selected disabled value="">Filter By Kategori</option>
                            <option>Teknologi</option>
                            <option>Industri</option>
                            <option>Politik</option>
                        </select>
                    </div>
                </div>
            </div>
    
            <div id="load_discuss" style="width:100%;"></div>
            <div id="load_comment" style="width:100%;">
            
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

		</div>
	</div>
</div>

<div class="modal fade" id="modal-add-disscussion">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Diskusi</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="add-disscussion" method="post">
                <div class="modal-body">
                        <div class="form-group">
                            <input class="form-control" name="nama" placeholder="Tulis Nama Disini..." required>
                        </div>
                        <div class="form-group">
                            <input class="form-control" name="judul" placeholder="Tulis Judul Diskusi Disini..." required>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control deskripsi" name="desc" placeholder="Deskripsi Disini...." required></textarea>
                        </div>
                        <div class="form-group">
                            <select class="form-control" name="kategori" required>
                                <option selected disabled value="">Kategori</option>
                                <option>Teknologi</option>
                                <option>Industri</option>
                                <option>Politik</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-add-comment">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Komentar</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="add-comment" method="post">
                <div class="modal-body">
                        <div class="form-group">
                            <input class="form-control" id="nama_pengirim" name="nama" placeholder="Tulis Nama Disini..." required>
                            <input type="hidden" id="id_diskusi" name="id_diskusi">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control deskripsi" id="my_komentar" name="komentar" placeholder="Tulis Komentar Anda Disini..." required></textarea>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-reply-comment">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Balas Komentar</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="reply-comment" method="post">
                <div class="modal-body">
                        <div class="form-group">
                            <input class="form-control" name="nama" placeholder="Tulis Nama Disini..." required>
                            <input type="hidden" id="id_komentar" name="id_komentar">
                            <input type="hidden" id="id_diskusi_rep" name="id_diskusi">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control deskripsi" name="komentar" placeholder="Tulis Komentar Anda Disini..." required></textarea>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
            </form>
        </div>
    </div>
</div>