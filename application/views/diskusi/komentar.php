<div class="content-header">
	<div class="container">
		<div class="row mb-2">
			<div class="col-sm-12">
				<h1 class="m-0 text-dark"> Comment <small>Page</small></h1>
			</div>
		</div>
	</div>
</div>

<div class="content">
	<div class="container">
		<div class="row">
            <div class="col-md-12 mb-3">

                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-1">
                                <img src="<?= base_url().'assets/dist/img/user1-128x128.jpg' ?>" class="img-circle img-fluid">
                            </div>
                            <div class="col-md-11">
                                <h6>Pak Budi <label class="text-scafol">- (28 - Juli - 2020)</label></h6>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Natus, repellendus voluptate, similique hic modi obcaecati magnam commodi delectus atque dignissimos officiis amet quis debitis id quidem ducimus voluptates velit! Animi.</p>
                                <p class="desc_komen">Lorem ipsum, dolor sit amet consectetur adipisicing elit. At, deleniti? Enim repellendus harum vitae, itaque doloribus modi quod neque delectus, cupiditate dignissimos, quibusdam veniam vero eveniet maiores amet nisi sapiente?Lorem ipsum dolor sit amet consectetur adipisicing elit. Adipisci, perspiciatis. Est consequuntur animi provident ducimus praesentium harum, maxime voluptatibus officiis quae distinctio, nostrum consequatur rem, eveniet quos accusantium sapiente! Impedit.</p>

                                <!-- reply -->

                            </div>
                        </div>
                    </div>
                </div>

            </div>
		</div>
	</div>
</div>