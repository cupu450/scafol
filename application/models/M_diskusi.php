<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_diskusi extends CI_Model {

    public function get_data($options = null)
    {
        if (!isset($options)) {
            $data = $this->db->get('diskusi');
        } else {
            $data = $this->db->get_where('diskusi', $options);
        }

        return $data->result();
    }

    public function get_like($options)
    {
        // $array = array('title' => $match, 'page1' => $match, 'page2' => $match);
        $this->db->like($options)
                 ->from('diskusi');
        $query = $this->db->get();

        return $query->result();
    }

    public function get_one_data($options)
    {
        $data = $this->db->get_where('diskusi', $options);
        return $data->first_row('array');
    }

    public function add_data($data)
    {
        $this->db->set($data);
        $this->db->insert('diskusi');
        return $this->db->affected_rows();
    }

}
