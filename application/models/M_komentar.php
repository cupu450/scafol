<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_komentar extends CI_Model {

    public function get_data($tabel, $options)
	{
		$data = $this->db->get_where($tabel, $options);
		return $data->result();
    }
    
    public function count_data($tabel, $where)
    {
        $data = $this->db->where($where)->get($tabel);
		return $data->num_rows();
    }

    public function add_data($data)
    {
        $this->db->set($data);
        $this->db->insert('komentar');
        return $this->db->affected_rows();
    }

}
