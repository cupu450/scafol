<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'diskusi';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
// $route['komentar/(:num)'] = 'diskusi/get_komentar/$1';
$route['get_data'] = 'diskusi/get_data';
$route['get_comment'] = 'diskusi/get_komentar';
$route['add_disscussion'] = 'diskusi/tambah_diskusi';
$route['add_comment'] = 'diskusi/tambah_komen';
$route['reply_comment'] = 'diskusi/balas_komen';
$route['gallery'] = 'gallery';