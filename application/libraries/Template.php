<?php

class Template
{
    function __construct(){
      $this->CI = & get_instance();
    }

    function display($template, $data=null){
      $data['_content'] = $this->CI->load->view($template, $data, true);
      $data['_navbar'] = $this->CI->load->view('_partials/navbar', $data, true);
      $data['_footer'] = $this->CI->load->view('_partials/footer', $data, true);
      $this->CI->load->view('/template.php', $data);
    }

}