<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		$data['gallery'] = array(
			[
				'image' => 'photo1.png',
				'tag' => 'Jalan Tol',
				'user' => 'Pak Budi',
				'desc' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Laudantium praesentium eveniet eligendi pariatur aliquam officiis ullam! Quasi esse necessitatibus accusantium fugit expedita eaque, ratione harum, dicta labore facere laborum repudiandae.'
			],
			[
				'image' => 'photo2.png',
				'tag' => 'Jembatan',
				'user' => 'Pak Soni',
				'desc' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Laudantium praesentium eveniet eligendi pariatur aliquam officiis ullam! Quasi esse necessitatibus accusantium fugit expedita eaque, ratione harum, dicta labore facere laborum repudiandae.'
			],
			[
				'image' => 'photo3.jpg',
				'tag' => 'Underpass',
				'user' => 'Pak Budi',
				'desc' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Laudantium praesentium eveniet eligendi pariatur aliquam officiis ullam! Quasi esse necessitatibus accusantium fugit expedita eaque, ratione harum, dicta labore facere laborum repudiandae.'
			],
			[
				'image' => 'photo4.jpg',
				'tag' => 'Jembatan',
				'user' => 'Pak Toto',
				'desc' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Laudantium praesentium eveniet eligendi pariatur aliquam officiis ullam! Quasi esse necessitatibus accusantium fugit expedita eaque, ratione harum, dicta labore facere laborum repudiandae.'
			]
		);
		$this->template->display('galeri', $data);
	}
}
