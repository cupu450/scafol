<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Diskusi extends CI_Controller {

    public $variabelku;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_komentar');
        $this->load->model('M_diskusi');
        $this->variabelku = "";
    }

    public function index()
    {
        $this->template->display('diskusi/index');
    }

    public function get_data()
    {
        if ($this->input->post('kategori') == '' && $this->input->post('judul') == '') {
            $discuss = $this->M_diskusi->get_data();
        } elseif($this->input->post('kategori') != ''){
            $discuss = $this->M_diskusi->get_data(array('kategori' => $this->input->post('kategori')));
        } else {
            $discuss = $this->M_diskusi->get_like(array('judul' => $this->input->post('judul')));
        }

        $data = [
            'success' => true,
            'data' => $discuss
        ];

        echo json_encode($data);
    }

    public function get_komentar()
    {
        $variabelku = "";
        $discuss = $this->M_diskusi->get_one_data(array('Id_diskusi' => $this->input->post('id')));
        $comment = $this->M_komentar->get_data('komentar', array('parent_komentar_id' => 0, 'id_diskusi' => $this->input->post('id')));
        
        $variabelku .= '
            <div class="col-md-12 mb-3">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <a href="#" class="text-scafol" onclick="GoBack();"><i class="fas fa-arrow-left"></i> Kembali</a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                                <img src="'.base_url().'assets/dist/img/'.$discuss['foto'].'" class="img-circle img-fluid img-comment">
                            </div>
                            <div class="col-md-11">
                                <div class="comment_discuss">
                                    <h6>Pak Budi <label class="text-scafol">- ('.date('d-m-Y', strtotime($discuss['tanggal'])).')</label></h6>
                                    <h4>'.$discuss['judul'].'</h4>
                                    '.$discuss['deskripsi'].'
                                </div>

                                <button class="btn btn-flat btn-scafol btn-block mb-3" onclick="CallCommentModal('.$discuss['Id_diskusi'].');">Berikan Komentar Anda</button>

                                <div class="card">
                                    <div class="card-body commentary">
                                        
                                    
        ';
        foreach($comment as $row)
        {
            $variabelku .= '
                <div class="row mb-5">
                    <div class="col-md-1">
                        <img src="'.base_url().'assets/dist/img/'.$row->foto.'" class="img-circle img-fluid img-comment">
                    </div>
                    <div class="col-md-11">
                        <h6>'.$row->nama_pengirim.' <label class="text-scafol">- ('.$row->tanggal.')</label></h6>
                        '.$row->komentar.'
                        <a href="#" class="text-scafol" onclick="CallReplyComment('.$row->Id_komentar.','.$discuss['Id_diskusi'].')" style="margin-bottom: 10px;">Balas</a>
                    </div>
                </div>
            ';

            $variabelku .= $this->get_reply($row->Id_komentar, 0, $discuss['Id_diskusi']);
        }

        echo json_encode([$variabelku]);
    }
    
    public function get_reply($parent_id = 0, $margin_left = 0, $id_diskusi = 0)
    {
        $variabelku = "";
        $sub_comment = $this->M_komentar->get_data('komentar', array('parent_komentar_id' => $parent_id));

        
        if ($this->M_komentar->count_data('komentar', array('parent_komentar_id' => $parent_id)) > 0) 
        {
            if ($parent_id == 0) 
            {
                $margin_left = 0;
            } else {
                $margin_left = $margin_left + 48;
            }

            foreach($sub_comment as $row)
            {
                $variabelku .= '
                    <div class="row mb-5" style="margin-left: '.$margin_left.'px">
                        <div class="col-md-1">
                            <img src="'.base_url().'assets/dist/img/'.$row->foto.'" class="img-circle img-fluid img-comment">
                        </div>
                        <div class="col-md-11">
                            <h6>'.$row->nama_pengirim.' <label class="text-scafol">- ('.$row->tanggal.')</label></h6>
                            <p style="margin-bottom: 0">'.$row->komentar.'</p>
                            <a href="#" class="text-scafol" onclick="CallReplyComment('.$row->Id_komentar.','.$id_diskusi.')" style="margin-bottom: 10px;">Balas</a>
                        </div>
                    </div>
                ';

                $variabelku .= $this->get_reply($row->Id_komentar, $margin_left, $id_diskusi);
            }
        }

        return $variabelku;
    }

    public function tambah_diskusi()
    {
        $foto = ['user1-128x128.jpg','user2-160x160.jpg','user3-128x128.jpg','user4-128x128.jpg','user8-128x128.jpg','user5-128x128.jpg'];
        $acakFoto = array_rand($foto);

        $data = array(
            'nama' => $this->input->post('nama'),
            'judul' => $this->input->post('judul'),
            'deskripsi' => $this->input->post('desc'),
            'kategori' => $this->input->post('kategori'),
            'foto' => $foto[$acakFoto],
        );

        $insert = $this->M_diskusi->add_data($data);

        if ($insert > 0) {
            $response = [
                'success' => true,
                'message' => 'Diskusi berhasil ditambahkan'
            ];
        } else {
            $response = [
                'success' => false,
                'message' => 'Diskusi gagal ditambahkan'
            ];
        }

        echo json_encode($response);
    }

    public function tambah_komen()
    {
        $foto = ['user1-128x128.jpg','user2-160x160.jpg','user3-128x128.jpg','user4-128x128.jpg','user8-128x128.jpg','user5-128x128.jpg'];
        $acakFoto = array_rand($foto);

        $data = array(
            'parent_komentar_id' => 0,
            'id_diskusi' => $this->input->post('id_diskusi'),
            'komentar' => $this->input->post('komentar'),
            'nama_pengirim' => $this->input->post('nama'),
            'foto' => $foto[$acakFoto],
            'tanggal' => date('Y-m-d')
        );

        $insert = $this->M_komentar->add_data($data);

        if ($insert > 0) {
            $response = [
                'success' => true,
                'message' => 'Komentar berhasil ditambahkan',
                'id_diskusi' => $this->input->post('id_diskusi')
            ];
        } else {
            $response = [
                'success' => false,
                'message' => 'Komentar gagal ditambahkan'
            ];
        }

        echo json_encode($response);
    }

    public function balas_komen()
    {
        $foto = ['user1-128x128.jpg','user2-160x160.jpg','user3-128x128.jpg','user4-128x128.jpg','user8-128x128.jpg','user5-128x128.jpg'];
        $acakFoto = array_rand($foto);

        $data = array(
            'parent_komentar_id' => $this->input->post('id_komentar'),
            'id_diskusi' => NULL,
            'komentar' => $this->input->post('komentar'),
            'nama_pengirim' => $this->input->post('nama'),
            'foto' => $foto[$acakFoto],
            'tanggal' => date('Y-m-d')
        );

        $insert = $this->M_komentar->add_data($data);

        if ($insert > 0) {
            $response = [
                'success' => true,
                'message' => 'Komentar berhasil ditambahkan',
                'id_diskusi' => $this->input->post('id_diskusi')
            ];
        } else {
            $response = [
                'success' => false,
                'message' => 'Komentar gagal ditambahkan'
            ];
        }

        echo json_encode($response);
    }

}
